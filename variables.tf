variable "network_name" {
  type = string
}

variable "network_mode" {
  type = string
}

variable "network_domain" {
  type = string
}

variable "network_subnet" {
  type = list(string)
}

variable "network_autostart" {
  type = bool
  default = true
}

variable "network_dhcp_enabled" {
  type = bool
  default = false
}

variable "network_dns_enabled" {
  type = bool
  default = false
}

variable "local_only" {
  type = bool
  default = false
}
